<?
parse_str($_SERVER['QUERY_STRING'],$_GET);
parse_str($_SERVER['POST'],$_POST);
$params=$_GET+$_POST;
if (!$params['domain']) {message("Error occured", "Domain name not specified"); exit;}

if (!extension_loaded('pcre')) dl('pcre.so');

$USER=$_SERVER['USER'];
$DOMAIN_DIR="{$_SERVER['HOME']}/domains/{$params['domain']}";
$CONF_DIR="$DOMAIN_DIR/.plugins/stats_control";
$STATS_DIR="$DOMAIN_DIR/stats";
$GLOBAL_DIR="{$_SERVER['HOME']}/../admin/.plugins/stats_control";

foreach (Array('awstats','awstats_static','webalizer') as $value) {
	$DENY[$value]=file_exists("$GLOBAL_DIR/policy/$value/$USER");
	if (file_exists("$GLOBAL_DIR/policy/deny_$value")) $DENY[$value]=(!$DENY[$value]);
}

extract(parse_ini_file("$GLOBAL_DIR/config.ini"),EXTR_OVERWRITE);

if (file_exists("$CONF_DIR/config.ini")) $CFG=parse_ini_file("$CONF_DIR/config.ini");
if (!$CFG['AW_CGI_PATH']) $CFG['AW_CGI_PATH']=$AW_DEFAULT_PATH;

function message($text,$details){
	echo '
<p><table width=50% height=100 cellspacing=0 cellpadding=5>
    <tr>
      <td height="50%" valign="middle" align="center">
        <p align="center">'.$text.'</p>
      </td>
    </tr>
    <tr>
    	<td height=1 valign="middle" align="center">
    		<table width = 50%>
    			<tr><td bgcolor="#C0C0C0"> </td></tr>
    		</table>
    	</td>
    </tr>
    <tr>
      <td height="50%" valign="middle" align="center">
      	<p align="center"><b>Details</b></p>
        <p align="center">'.$details.'</p>
      </td>
    </tr>
</table>
</p>
';
	exit;
}

function mkdir_r($path,$mode=FALSE){
	if (file_exists($path) && is_dir($path)) return 1;
	else {
		$dir = substr($path,0,strrpos($path, '/'));
		if (mkdir_r($dir))
			if ($mode==FALSE) return mkdir($path);
			else return mkdir($path,$mode);
	}
}

function options($options,$selected='',$firstempty=' !@#',$skip=' !@#'){
	if ($firstempty!=' !@#' && ($firtempty || $firstempty==$selected)) $result='<option></option>';
	while (list($key,$val)=each($options)) if ($skip!="$key")
		$result.="\n<option value='$key'".($selected=="$key"?' selected':'').">$val</option>";
	return $result;
}

$PAGE_FOOTER="<br>\nStats Control Plugin &copy; 2005 <a href=http://ruweb.net>RuWeb, JSC</a></center>";
echo "<center><br><a href='index.html?domain={$params['domain']}'><b>Stats Control Plugin</b></a><br>\n";
