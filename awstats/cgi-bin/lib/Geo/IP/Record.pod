=head1 NAME

  Geo::IP::Record - Contains city information for GeoIP City Edition

=head1 SYNOPSIS

  use Geo::IP;

  my $gi = Geo::IP->open("/usr/local/share/GeoIP/GeoIPCity.dat", GEOIP_STANDARD);

  my $record = $gi->record_by_addr('24.24.24.24');

  print $record->country_code,
	$record->country_code3,
	$record->country_name,
	$record->region,
	$record->city,
	$record->postal_code,
	$record->latitude,
	$record->longitude,
	$record->dma_code,
	$record->area_code;

=head1 DESCRIPTION

Geo::IP::Record represents objects containing location information
returned by the GeoIP City database.

Note at this time only a commercial version - GeoIP City Premium - is available
from MaxMind.  The database that is distributed free of charge 
is the GeoIP Standard Country, and is not compatible with this API.

At some point in the future, we may offer a free version - GeoIP City Standard -
based only on public data sources such as RIR Whois databases.

=head1 OBJECT METHODS

=over 4

=item $code = $record->country_code;

Returns the ISO 3166 country code from the location object.

=item $code3 = $record->country_code3;

Returns the ISO 3166 3 letter country code from the location object.

=item $name = $record->country_name;

Returns the country name from the location object.

=item $region = $record->region;

Returns the region from the location object.

=item $city = $record->city;

Returns the city from the location object.

=item $postal_code = $record->postal_code;

Returns the postal code from the location object.

=item $lat = $record->latitude;

Returns the latitude from the location object.

=item $lon = $record->longitude;

Returns the longitude from the location object.

=item $dma_code = $record->dma_code;

Returns the DMA code from the location object (for city-level US locations only)

=item $area_code = $record->area_code;

Returns the area code from the location object (for city-level US locations only)

=head1 VERSION

1.20

=head1 AUTHOR

Copyright (c) 2003, MaxMind LLC

All rights reserved.  This package is free software; you can redistribute it
and/or modify it under the same terms as Perl itself.

=cut
