#!/bin/sh

export CONFIG="/etc/webalizer/$HOSTNAME.conf"
export DOM_CONF=".plugins/stats_control/webalizer.conf"

if [ ! -e "$CONFIG" \
    -o "$DOM_DIR/$DOM_CONF" -nt "$CONFIG" \
    -o "$DOM_DIR" -nt "$CONFIG" \
    -o "$CONF_DIR/wb_template.conf" -nt "$CONFIG" \
    -o "$CONF_DIR/wb_override.conf" -nt "$CONFIG" \
        ]; then
    cat "$CONF_DIR/wb_template.conf" > "$CONFIG.tmp"
    echo >> "$CONFIG.tmp"
    if [ -s "$DOM_DIR/$DOM_CONF" ]; then
	grep -vi DumpPath "$DOM_DIR/$DOM_CONF" >> "$CONFIG.tmp"
    else
	cat "$CONF_DIR/webalizer.conf.default" >> "$CONFIG.tmp"
    fi
    echo >> "$CONFIG.tmp"
    cat "$CONF_DIR/wb_override.conf" >> "$CONFIG.tmp"
    sed -e "s#|USER|#$USER#g;s#|DOMAIN|#$DOMAIN#g;s#|HOST|#$HOSTNAME#g;s#|LOG|#$LOG#g;s#|OUT_DIR|#$OUT_DIR#g" "$CONFIG.tmp" > "$CONFIG" && \
    rm "$CONFIG.tmp"
    chown -h :$USER "$CONFIG" && chmod 640 "$CONFIG"
    if [ -d "$DOM_DIR/stats" ]; then chown -hR $USER:$USER "$DOM_DIR/stats/"; fi
#elif [ 0$VER -ne 2 ]; then
#    chown -h $USER:$USER "$OUT_DIR" "$DOM_DIR/stats/"
fi

