#!/bin/sh

exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"

[ -z "$DOCUMENT_ROOT" ] && DOCUMENT_ROOT="./"
cd $DOCUMENT_ROOT

chmod 700 install.sh webalize_wrapper.sh webalizer_root.sh awstats_root.sh
chown -hR root:wheel ../scripts/
chown -hR diradmin:diradmin ../admin/ ../user/

PLUGIN_DIR=/usr/local/directadmin/plugins/Stats_Control
mkdir -m711 /home/admin/.plugins /etc/awstats /etc/webalizer
mkdir /var/www/html/awstatsicons

set -xe

cp -R "$PLUGIN_DIR/scripts/.plugins/stats_control" /home/admin/.plugins/
chown -hR admin:admin /home/admin/.plugins

cp $PLUGIN_DIR/scripts/awstats.conf /etc/awstats/awstats.conf

#mv -n /usr/local/bin/webalizer /usr/local/bin/webalizer_default
#ln -fs "$PLUGIN_DIR/scripts/webalize_wrapper.sh" /usr/local/bin/webalizer

#ln -fs /usr/local/bin/webalizer /usr/local/bin/webalizer_default

cp -R $PLUGIN_DIR/awstats/icons/* /var/www/html/awstatsicons/

perl -pi -e 's/(webalizer|rotation)=1/${1}=0/' /usr/local/directadmin/conf/directadmin.conf
echo webalizer=0 >> /usr/local/directadmin/conf/directadmin.conf
echo rotation=0 >> /usr/local/directadmin/conf/directadmin.conf

echo -e "40 0 * * * root /usr/local/directadmin/plugins/Stats_Control/scripts/stats_run.sh #Added by Stats Control Plugin" >> /etc/crontab

set +xe
grep -cm1 'Alias /awstatsicons/' /etc/httpd/conf/httpd.conf >/dev/null
if [ $? -ne 0 ]; then
	perl -pi -e 's#^(Alias /squirrelmail /var/www/html/squirrelmail/\s*)#${1}\nAlias /awstatsicons/ /var/www/html/awstatsicons/\n#' /etc/httpd/conf/httpd.conf
	if [ -e "/usr/local/etc/rc.d/httpd" ]; then
		/usr/local/etc/rc.d/httpd reload
	else
		service httpd reload
	fi
fi
echo "Plugin Installed!";

. update.sh

echo "</PRE>"

exit 0;
