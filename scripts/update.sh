#!/bin/sh

exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"

[ -z "$DOCUMENT_ROOT" ] && DOCUMENT_ROOT="./"
cd $DOCUMENT_ROOT

chmod 700 install.sh webalize_wrapper.sh webalizer_root.sh awstats_root.sh
chown -hR root:wheel ../scripts/
chown -hR diradmin:diradmin ../admin/ ../user/

touch /home/admin/.plugins/stats_control/*template.conf

if [ -d /usr/local/www/awstats/cgi-bin ]; then
    replace /usr/local/directadmin/plugins/Stats_Control/awstats/ /usr/local/www/awstats/ \
        -- /home/admin/.plugins/stats_control/config.ini /home/admin/.plugins/stats_control/*.conf
    if [ ! -L /var/www/html/awstatsicons ] || [ ! -d /var/www/html/awstatsicons/ ]; then
        mv /var/www/html/awstatsicons /var/www/html/awstatsicons~
        ln -s /usr/local/www/awstats/icon /var/www/html/awstatsicons
    fi
fi

cd /var/log/httpd/domains/ && rm -f *[^lt][^oe][^gs]

echo "Plugin has been updated!"; #NOT! :)

exit 0;
