#!/bin/sh
exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"
set -x

perl -ni -e 'unless (m/#Added by Stats Control Plugin$/) { print }' /etc/crontab 2>&1
perl -pi -e 's/(webalizer|rotation)=0/${1}=1/' /usr/local/directadmin/conf/directadmin.conf
rm /etc/awstats.conf
set +x

echo "Plugin Un-Installed!"; #NOT! :)

exit 0;
