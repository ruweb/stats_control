#!/bin/sh

if [ ! -e "$OUT_DIR" ]; then
    mkdir "$OUT_DIR"
fi
cd "$OUT_DIR"

exec >"$OUT_DIR/awstats.log" 2>&1
if [ -e "$DOM_DIR/.plugins/stats_control/aw_static" ] \
    && [ ! -e "$CONF_DIR/policy/deny_awstats_static" -a ! -e "$CONF_DIR/policy/awstats_static/$USER" \
	-o -e "$CONF_DIR/policy/deny_awstats_static" -a -e "$CONF_DIR/policy/awstats_static/$USER" ]; then
    set -x
    HTTP_HOST="$DOMAIN" $AW_BSP \
        -config=$HOSTNAME \
        -awstatsprog=$AW_PROG \
        -dir=$OUT_DIR \
        -builddate=%YYYY.%MM \
        -update
else
    set -x
    HTTP_HOST="$DOMAIN" "$AW_PROG" "-config=$HOSTNAME" -update
fi
