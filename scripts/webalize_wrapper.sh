#!/bin/sh

#exec &>/dev/null
#exec <&-

WEBALIZER_LOG="/var/log/webalize_wrapper.log"
CONF_DIR="/home/admin/.plugins/stats_control"

log(){
    echo -e "`date '+%F %R:%S'` - $1" >> $WEBALIZER_LOG;
}

load_value(){
    if [ -s "$1" ]; then
        VALUE=`cat "$1"`
    else
	VALUE=$2
    fi
}

if [ $# -ge 8 -a -s "$CONF_DIR/config.ini" ]; then

#    log "ARGS: $*"
    
    . "$CONF_DIR/config.ini"
    export CONF_DIR AW_BSP AW_PROG CONFIG=$2 HOST=$5 OUT_DIR=$7 LOG=$8
    
    if [ "0$NICE" -gt 0 ]; then
	renice "0$NICE" -p $$
    fi
#-c /usr/local/directadmin/data/templates/custom/webalizer.conf.host9.ruweb.net -p -n host9.ruweb.net -o /home/stats/domains/host9.ruweb.net/stats /var/log/httpd/domains/host9.ruweb.net.log
    
    if [ "${OUT_DIR%%*/domains/*/stats}" ]; then
	#it's subdomain
        DOM_DIR=${OUT_DIR%/stats/*}
    else
	#it's domain
        DOM_DIR=${OUT_DIR%/stats}
    fi
    DOMAIN=${DOM_DIR##*/}
    HOME_DIR=${DOM_DIR%/domains/*}
    USER=${HOME_DIR#/*/}
    
    if [ ! -n "$USER" ]; then
	log "ERROR: Failed to find USER from OUT_DIR=$OUT_DIR"
    fi

    export USER DOMAIN HOME_DIR DOM_DIR

#    log "Found USER=$USER, DOMAIN=$DOMAIN, HOST=$HOST, LOG=$LOG, HOME_DIR=$HOME_DIR, OUT_DIR=$OUT_DIR"

    load_value "$DOM_DIR/.plugins/stats_control/type" "$DEFAULT_TYPE"
    TYPE=$VALUE

    if [ "$TYPE" = "webalizer" -o "$TYPE" = "awstats" ]; then
	if [ -e "$CONF_DIR/policy/deny_$TYPE" ]; then
	    if [ ! -e "$CONF_DIR/policy/$TYPE/$USER" ]; then
		TYPE="none"
	    fi
	elif [ -e "$CONF_DIR/policy/$TYPE/$USER" ]; then
		TYPE="none"
	fi

#        log "Running $TYPE for $HOST"
	. "/usr/local/directadmin/plugins/Stats_Control/scripts/${TYPE}_root.sh"
        su -m $USER -c "/usr/local/directadmin/plugins/Stats_Control/scripts/${TYPE}_user.sh"
        exit $?
    fi

    exit

else 
    log "ERROR: Wrong input parameters count - $#"
fi

/usr/local/bin/webalizer_default $*
exit $?

#created='stat -f%B'