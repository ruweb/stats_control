#!/bin/sh

#exec &>/dev/null
#exec <&-
#set -x

MY_LOG="/var/log/stats_run.log"
DOMLOGS_DIR_REAL="/var/log/httpd/domains"
DOMLOGS_DIR=$DOMLOGS_DIR_REAL
SCRIPTS_DIR="/usr/local/directadmin/plugins/Stats_Control/scripts"
CONF_DIR="/home/admin/.plugins/stats_control"
USER_DIR='/usr/local/directadmin/data/users'

#defaults
AW_PROG="/usr/local/directadmin/plugins/Stats_Control/awstats/cgi-bin/awstats.pl"
AW_SCRIPT="/usr/local/directadmin/plugins/Stats_Control/awstats/cgi-bin/awstats.pl"
AW_DIR="/usr/local/directadmin/plugins/Stats_Control/awstats/cgi-bin"
AW_DEFAULT_PATH="cgi-bin/awstats"
AW_BSP="/usr/local/directadmin/plugins/Stats_Control/scripts/awstats_buildstaticpages.pl"
DEFAULT_TYPE="webalizer"
NICE="10"
WB_LANGS="default:russian"
WB_BIN_DIR="/usr/local/bin"
ROTATE_SIZE="204800"
ROTATE_DAY="1"

. "$CONF_DIR/config.ini"

if [ "0$NICE" -gt 0 ]; then
	renice "0$NICE" -p $$ 2>/dev/null 1>&2
fi

if [ ! -d "$DOMLOGS_DIR/.rotate/.rotate" ]; then
    mkdir -p "$DOMLOGS_DIR/.rotate/.rotate"
fi

export CONF_DIR AW_BSP AW_PROG VER=2

log(){
    echo -e "`date '+%F %R:%S'` - $1" >> $MY_LOG;
}

load_value(){
    if [ -s "$1" ]; then
        VALUE=`cat "$1"`
    else
	VALUE=$2
    fi
}

process(){
#    export CONF_DIR AW_BSP AW_PROG CONFIG=$2 HOSTNAME=$5 OUT_DIR=$7 LOG=$8
#    export USER DOMAIN HOME_DIR DOM_DIR

    log "Running $TYPE for $HOSTNAME"
    . "$SCRIPTS_DIR/${TYPE}_root.sh"
    export USER DOMAIN HOSTNAME HOME_DIR DOM_DIR OUT_DIR LOG ROTATE_DIR
    (su -m "$USER" -c "$SCRIPTS_DIR/${TYPE}_user.sh" 2>/dev/null)
    return $?
}

move_dom_logs(){
#$1=/path/domain
#$2=target_dir
#$3=user
    mv $1.log $1.error.log $2
    touch $1.log $1.error.log
    chown -h nobody:$3 $1.log $1.error.log
}

if [ 0`date +%d` -eq 0$ROTATE_DAY -o "$1" = "-r" ]; then
	ROTATE=1;
	ROTATE_DATE=`date '+%F'`
        ROTATE_DIR='.rotate/.rotate';
	for FILE in $DOMLOGS_DIR/*.log; do
	    mv $FILE $DOMLOGS_DIR/.rotate/
	done
	DOMLOGS_DIR="$DOMLOGS_DIR/.rotate"
	killall -USR1 -u root httpd
	killall -USR1 -u root nginx 2>/dev/null
	sleep 60
	gzip -f $MY_LOG
else
	ROTATE=0;
fi

for USER in `ls -1 $USER_DIR`; do
    if [ -s $USER_DIR/$USER/domains.list ]; then

#	if [ 0`grep -c suspended=yes $USER_DIR/$USER/user.conf` -gt 0 ]; then
	if [ 0`grep -ci account=OFF $USER_DIR/$USER/user.conf` -gt 0 ]; then
		log "Skipping user $USER."
		continue
	fi
	for DOMAIN in `cat "$USER_DIR/$USER/domains.list"`; do
	
	    if [ ! -d "/home/$USER/domains/$DOMAIN" ]; then continue; fi;

#	    echo $USER:$DOMAIN;
	    SIZE=`du -ks $DOMLOGS_DIR/$DOMAIN.*log 2>/dev/null | awk '{ total += $1 } END { print total }'`
	    if [ 0$SIZE -eq 0 ]; then
		if [ 0$ROTATE -ne 0 ]; then
		    rm $DOMLOGS_DIR/$DOMAIN.*log
		fi
		continue
	    fi

	    HOME_DIR="/home/$USER"
	    DOM_DIR="/home/$USER/domains/$DOMAIN"
		
	    SUBDOMAINS=`cat "$USER_DIR/$USER/domains/$DOMAIN.subdomains"`;

	    if [ $ROTATE -ne 0 ]; then
		mv $DOMLOGS_DIR/$DOMAIN.log $DOMLOGS_DIR/$DOMAIN.error.log $DOMLOGS_DIR_REAL/$ROTATE_DIR/
		chown -h nobody:$USER $DOMLOGS_DIR/../$DOMAIN.log $DOMLOGS_DIR/../$DOMAIN.error.log
		chmod 440 $DOMLOGS_DIR/../$DOMAIN.log $DOMLOGS_DIR/../$DOMAIN.error.log
		
		if [ -s $USER_DIR/$USER/domains/$DOMAIN.subdomains ]; then
		    for SUB in $SUBDOMAINS; do
			mv $DOMLOGS_DIR/$DOMAIN.$SUB.log $DOMLOGS_DIR/$DOMAIN.$SUB.error.log $DOMLOGS_DIR_REAL/$ROTATE_DIR/
			chown -h nobody:$USER $DOMLOGS_DIR/../$DOMAIN.$SUB.log $DOMLOGS_DIR/../$DOMAIN.$SUB.error.log
			chmod 440 $DOMLOGS_DIR/../$DOMAIN.$SUB.log $DOMLOGS_DIR/../$DOMAIN.$SUB.error.log
		    done
		fi
		
	    elif [ 0$SIZE -gt $ROTATE_SIZE ]; then
		ROTATE_DATE=`date '+%F'`
		ROTATE_DIR='.rotate/.rotate';
		umask 0227
		move_dom_logs "$DOMLOGS_DIR/$DOMAIN" "$DOMLOGS_DIR_REAL/$ROTATE_DIR/" $USER
		
		if [ -s $USER_DIR/$USER/domains/$DOMAIN.subdomains ]; then
		    for SUB in $SUBDOMAINS; do
		        move_dom_logs "$DOMLOGS_DIR/$DOMAIN.$SUB" "$DOMLOGS_DIR_REAL/$ROTATE_DIR/" $USER
		    done
		fi
		killall -USR1 -u root httpd
		killall -USR1 -u root nginx 2>/dev/null
		umask 0022
	    else
		ROTATE_DIR='';
	    fi


	    load_value "$DOM_DIR/.plugins/stats_control/type" "$DEFAULT_TYPE"
	    TYPE=$VALUE

	    if [ "$TYPE" = "webalizer" -o "$TYPE" = "awstats" ]; then
		if [ -e "$CONF_DIR/policy/deny_$TYPE" ]; then
		    if [ ! -e "$CONF_DIR/policy/$TYPE/$USER" ]; then
			TYPE="none"
		    fi
		elif [ -e "$CONF_DIR/policy/$TYPE/$USER" ]; then
		    TYPE="none"
		fi
			
		if [ "$TYPE" != "none" ]; then

		    if [ -s "$DOMLOGS_DIR_REAL/$ROTATE_DIR/$DOMAIN.log" ]; then
			    HOSTNAME=$DOMAIN
    			    if [ "$TYPE" = "awstats" ]; then
				LOG="$DOMLOGS_DIR_REAL/__ROTATE_DIR__/$DOMAIN.log"
	    		    else 
				LOG="$DOMLOGS_DIR_REAL/$ROTATE_DIR/$DOMAIN.log"
			    fi
			    OUT_DIR="$DOM_DIR/stats"
		    
			    process
		    fi

		    if [ -s "$USER_DIR/$USER/domains/$DOMAIN.subdomains" ]; then
			for SUB in $SUBDOMAINS; do

#			    echo $USER:$SUB.$DOMAIN;
			    if [ ! -s "$DOMLOGS_DIR_REAL/$ROTATE_DIR/$DOMAIN.$SUB.log" ]; then
				continue
			    fi
			    HOSTNAME="$SUB.$DOMAIN"
			    if [ "$TYPE" = "awstats" ]; then
				LOG="$DOMLOGS_DIR_REAL/__ROTATE_DIR__/$DOMAIN.$SUB.log"
			    else 
				LOG="$DOMLOGS_DIR_REAL/$ROTATE_DIR/$DOMAIN.$SUB.log"
			    fi
			    OUT_DIR="$DOM_DIR/stats/$SUB"

			    process
			done
		    fi
		else
		    log "Skipping $DOMAIN. Running $TYPE is denied."
		fi
	    else
		log "Skipping $DOMAIN."
	    fi

    	    if [ -n "$ROTATE_DIR" ]; then
		log "Rotating logs for $DOMAIN."
		TARGET="$DOM_DIR/logs/$ROTATE_DATE"
		if [ ! -e "$DOM_DIR/logs/" ]; then
		    mkdir $DOM_DIR/logs/ && chown -h :wheel $DOM_DIR/logs/
		elif [ -e "$TARGET.tar.gz" ]; then
		    I=`ls -dc1 $TARGET*.tar.gz | grep -c .`
		    TARGET="$DOM_DIR/logs/$ROTATE_DATE($I)"
		fi
		cd "$DOMLOGS_DIR_REAL/$ROTATE_DIR" && tar czf "$TARGET.tar.gz" $DOMAIN.*log && rm -f $DOMAIN.*log
	    fi

	done
    fi
done

if [ $ROTATE -ne 0 ]; then
    ls -d $DOMLOGS_DIR/*.log 1>/dev/null 2>&1
    if [ $? -eq 0 ]; then
	LOGS=`ls -drc1 $DOMLOGS_DIR/domains.*.tar.gz 2>/dev/null`
	if [ $? -eq 0 ]; then
	    I=$[`echo $LOGS | wc -w`]
	    for FILE in $LOGS; do
		mv $FILE $DOMLOGS_DIR/domains.$I.tgz
		I=$[${I}-1]
	    done
	fi
	cd "$DOMLOGS_DIR" && tar czf domains.0.tgz *.log && rm -f *.log
    fi        
fi