#!/bin/sh

if [ ! -e "$OUT_DIR" ]; then
    mkdir "$OUT_DIR"
elif [ ! -s "$OUT_DIR/webalizer.current" -a -e "$OUT_DIR/webalizer.current" ]; then
    rm "$OUT_DIR/webalizer.current"
fi

if [ -s "$DOM_DIR/.plugins/stats_control/webalizer_lang" ]; then
    LANG=`cat "$DOM_DIR/.plugins/stats_control/webalizer_lang"`
    if [ ! -x "/usr/local/bin/webalizer_$LANG" ]; then
	LANG="default"
    fi
else
    LANG="default"
fi

[ "$LANG" = "default" ] && WB_PROG="/usr/local/bin/webalizer" || WB_PROG="/usr/local/bin/webalizer_$LANG"

exec >"$OUT_DIR/webalizer.log" 2>&1
set -x
"$WB_PROG" -c "$CONF_DIR/wb_default.conf" -c "$CONFIG" "$LOG" </dev/null && cp "$OUT_DIR/index.html" "$OUT_DIR/webalizer.html"
if [ $? -ne 0 ]; then
    set +x
    if [ "0`grep -cG '^.*: .* (12)$' $OUT_DIR/webalizer.log`" -ne 0 ]; then
	mv "$OUT_DIR/webalizer.current" "$OUT_DIR/webalizer.bak"
    fi
fi
