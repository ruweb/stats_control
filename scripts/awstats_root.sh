#!/bin/sh

export CONFIG="/etc/awstats/awstats.$HOSTNAME.conf"
export DOM_CONF=".plugins/stats_control/awstats.conf"

if [ ! -e "$CONFIG" \
    -o "$DOM_DIR/$DOM_CONF" -nt "$CONFIG" \
    -o "$DOM_DIR" -nt "$CONFIG" \
    -o "$CONF_DIR/aw_template.conf" -nt "$CONFIG" ]; then
    echo -e "Include \"$CONF_DIR/aw_default.conf\"\n" > "$CONFIG.tmp"
    if [ -s "$DOM_DIR/$DOM_CONF" ]; then
	grep -viE '(LoadPlugin|Include[^A-Z])' "$DOM_DIR/$DOM_CONF" >> "$CONFIG.tmp"
    fi
    cat "$CONF_DIR/aw_template.conf" >> "$CONFIG.tmp" && \
    echo -e "\nInclude \"$CONF_DIR/aw_override.conf\"" >> "$CONFIG.tmp" && \
    sed -e "s#|USER|#$USER#g;s#|DOMAIN|#$DOMAIN#g;s#|HOST|#$HOSTNAME#g;s#|LOG|#$LOG#g;s#|OUT_DIR|#$OUT_DIR#g;s#|DOM_DIR|#$DOM_DIR#g" "$CONFIG.tmp" > "$CONFIG" && \
    rm "$CONFIG.tmp"
    chown -h root:$USER "$CONFIG" && chmod 640 "$CONFIG"
    if [ -d "$DOM_DIR/stats" ]; then chown -hR $USER:$USER "$DOM_DIR/stats/"; fi
#elif [ 0$VER -ne 2 ]; then
#	chown -h $USER:$USER "$OUT_DIR" "$DOM_DIR/stats/"
fi
